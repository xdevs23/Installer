package me.deftware.emc.installer;

import com.google.gson.JsonObject;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import me.deftware.emc.installer.ui.InstallerUI;
import me.deftware.emc.installer.utils.Cleaner;
import me.deftware.emc.installer.utils.Utils;
import me.deftware.emc.installer.utils.WebUtils;

import javax.swing.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {
    public static String customEMC = "", customAristois = "";
    public static boolean optifine = true;
    public static String dataURL = "https://gitlab.com/EMC-Framework/maven",
            dataRawURL = dataURL + "/raw/master",
            marketplaceURL = dataRawURL + (!dataRawURL.isEmpty() && dataRawURL.endsWith("/") ? "" : "/") + "marketplace/index.json",
            aristoisURL = "https://aristois.net/maven/update.json";
    private static String versionsURL = dataRawURL + (!dataRawURL.isEmpty() && dataRawURL.endsWith("/") ? "" : "/") + "versions.json";

    public static void main(String[] args) {
        try {
            OptionParser optionParser = new OptionParser();
            optionParser.allowsUnrecognizedOptions();

            OptionSpec<String> emcVersion = optionParser.accepts("emc").withOptionalArg();
            OptionSpec<String> aristoisVersion = optionParser.accepts("aristois").withOptionalArg();
            OptionSpec<String> customDataURL = optionParser.accepts("dataurl").withOptionalArg();
            OptionSpec<String> customAristoisURL = optionParser.accepts("aristoisurl").withOptionalArg();

            OptionSpec<Boolean> devel = optionParser.accepts("dev").withOptionalArg().ofType(Boolean.class).describedAs("bool");
            OptionSpec<Boolean> optifine = optionParser.accepts("of").withOptionalArg().ofType(Boolean.class).describedAs("bool");
            OptionSpec<Boolean> clean = optionParser.accepts("clean").withOptionalArg().ofType(Boolean.class).describedAs("bool");
            OptionSpec<Boolean> deepclean = optionParser.accepts("deepclean").withOptionalArg().ofType(Boolean.class).describedAs("bool");

            OptionSet optionSet = optionParser.parse(args);
            if (optionSet.has(emcVersion)) {
                System.out.println("Using custom EMC version " + optionSet.valueOf(emcVersion));
                customEMC = optionSet.valueOf(emcVersion);
            }
            if (optionSet.has(customDataURL)) {
                System.out.println("Using custom data URL: " + optionSet.valueOf(customDataURL));
                dataURL = optionSet.valueOf(customDataURL);

                dataRawURL = dataURL + "/raw/master";
                versionsURL = dataRawURL + (!dataRawURL.isEmpty() && dataRawURL.endsWith("/") ? "" : "/") + "versions.json";

                System.out.println("Using custom raw data url: " + dataRawURL);
                System.out.println("Using custom Version Check URL (From Data URL): " + versionsURL);
            }
            if (optionSet.has(customAristoisURL)) {
                System.out.println("Using custom Aristois URL: " + optionSet.valueOf(customAristoisURL));
                aristoisURL = optionSet.valueOf(customAristoisURL);
            }

            if (optionSet.has(devel)) {
                System.out.println("Using development json");
                versionsURL = dataRawURL + (!dataRawURL.isEmpty() && dataRawURL.endsWith("/") ? "" : "/") + "versions-devel.json";
            }

            if (optionSet.has(aristoisVersion)) {
                System.out.println("Using custom Aristois version " + optionSet.valueOf(aristoisVersion));
                customAristois = optionSet.valueOf(aristoisVersion);
            }
            if (optionSet.has(optifine)) {
                System.out.println(optionSet.valueOf(optifine) ? "Using OptiFine" : "Installer will not install OptiFine");
                Main.optifine = optionSet.valueOf(optifine);
            }
            if (optionSet.has(clean) || optionSet.has(deepclean)) {
                Cleaner.clean(optionSet.has(deepclean));
            }
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            new Thread(() -> {
                try {
                    System.out.print("Fetching json files... ");
                    JsonObject emcJson = Utils.getJsonDataAsObject(WebUtils.get(versionsURL)),
                            aristoisJson = Utils.getJsonDataAsObject(WebUtils.get(aristoisURL)),
                            marketplaceJson = Utils.getJsonDataAsObject(WebUtils.get(marketplaceURL));
                    System.out.println("Done");
                    InstallerUI.create(emcJson, aristoisJson, marketplaceJson).setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getResponseCode(String websiteURL) throws IOException {
        if (!websiteURL.isEmpty()) {
            URL u = new URL(websiteURL);
            HttpURLConnection huc = (HttpURLConnection) u.openConnection();
            huc.setRequestMethod("HEAD");
            huc.connect();
            return huc.getResponseCode();
        } else {
            return HttpURLConnection.HTTP_NOT_FOUND;
        }
    }

    public static String trimEnd(String value, char characterToRemove) {
        int len = value.length();
        int st = 0;
        while ((st < len) && ((characterToRemove != ' ' && value.charAt(len - 1) == characterToRemove) || (Character.isWhitespace(value.charAt(len - 1))))) {
            len--;
        }
        return value.substring(0, len);
    }

}
