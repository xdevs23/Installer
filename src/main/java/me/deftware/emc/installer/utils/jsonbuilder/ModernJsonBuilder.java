package me.deftware.emc.installer.utils.jsonbuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.Date;

/**
 * Used to generate launcher profile json files for Minecraft 1.13 and higher
 */
public class ModernJsonBuilder extends AbstractJsonBuilder {

    public ModernJsonBuilder(JsonObject emcJson) {
        super(emcJson);
    }

    @Override
    public JsonObject build(String mcVersion, String emcVersion, String launcherString, String modLoaderString, String id, String emcTweaker, String inheritsFrom) {
        String date = formatDate(new Date());
        JsonObject jsonObject = new JsonObject();
        JsonArray libsArray = new JsonArray();

        if (launcherString.equalsIgnoreCase("vanilla")) {
            // Properties
            jsonObject.add("inheritsFrom", new JsonPrimitive(inheritsFrom));
            jsonObject.add("id", new JsonPrimitive(mcVersion + "-" + id));
            jsonObject.add("time", new JsonPrimitive(date));
            jsonObject.add("releaseTime", new JsonPrimitive(date));
            jsonObject.add("type", new JsonPrimitive("release"));
            // Tweaker
            JsonObject arguments = new JsonObject();
            JsonArray tweakArray = new JsonArray();
            tweakArray.add("--tweakClass");
            tweakArray.add(emcTweaker);
            if (modLoaderString.equalsIgnoreCase("rift")) {
                // Add Rift's TweakClass if Using Rift
                tweakArray.add("--tweakClass");
                tweakArray.add("org.dimdev.riftloader.launch.RiftLoaderClientTweaker");
            }
            arguments.add("game", tweakArray);
            jsonObject.add("arguments", arguments);
            jsonObject.add("mainClass", new JsonPrimitive("net.minecraft.launchwrapper.Launch"));
        } else if (launcherString.equalsIgnoreCase("multimc")) {
            jsonObject.add("formatVersion", new JsonPrimitive(1));

            JsonArray tweakerArray = new JsonArray();

            tweakerArray.add(emcTweaker);
            if (modLoaderString.equalsIgnoreCase("rift")) {
                // Add Rift's TweakClass if Using Rift
                tweakerArray.add("org.dimdev.riftloader.launch.RiftLoaderClientTweaker");
            }
            jsonObject.add("+tweakers", tweakerArray);
        }

        // Libraries
        libsArray.add(generateMavenRepo("name", "net.minecraft:launchwrapper:1.12", "", ""));
        libsArray.add(generateMavenRepo("name", "me.deftware:EMC:" + emcVersion, "url",
                "https://gitlab.com/EMC-Framework/maven/raw/master/"));
        libsArray.add(generateMavenRepo("name", "org.dimdev:mixin:0.7.11-SNAPSHOT", "url",
                "https://www.dimdev.org/maven/"));
        libsArray.add(generateMavenRepo("name", "net.jodah:typetools:0.5.0", "url",
                "https://repo.maven.apache.org/maven2/"));
        if (modLoaderString.equalsIgnoreCase("rift")) {
            // Rift ModLoader Dependencies
            if (!mcVersion.equals("1.13")) {
                // Custom Rift Jar containing Patch for KeyBinds for 1.13.2
                libsArray.add(generateMavenRepo("name", "com.github.CDAGaming:Rift:jitpack-0a2217b941-1", "url",
                        "https://www.jitpack.io/"));
            } else {
                libsArray.add(generateMavenRepo("name", "org.dimdev:rift:1.0.4-66", "url",
                        "https://www.dimdev.org/maven/"));
            }
            libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm:6.2", "url",
                    "http://repo1.maven.org/maven2/"));
            libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-commons:6.2", "url",
                    "http://repo1.maven.org/maven2/"));
            libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-tree:6.2", "url",
                    "http://repo1.maven.org/maven2/"));
        }

        if (launcherString.equalsIgnoreCase("vanilla")) {
            jsonObject.add("libraries", libsArray);
        } else if (launcherString.equalsIgnoreCase("multimc")) {
            jsonObject.add("+libraries", libsArray);
        }

        // Extra Properties (Or Other Properties Required by Other Launchers)
        if (launcherString.equalsIgnoreCase("multimc")) {
            jsonObject.add("mainClass", new JsonPrimitive("net.minecraft.launchwrapper.Launch"));
            jsonObject.add("name", new JsonPrimitive(id));
            jsonObject.add("releaseTime", new JsonPrimitive(date));

            JsonArray requiresArray = new JsonArray();
            JsonObject requiresData = new JsonObject();

            requiresData.add("equals", new JsonPrimitive(mcVersion));
            requiresData.add("uid", new JsonPrimitive("net.minecraft"));
            requiresArray.add(requiresData);
            jsonObject.add("requires", requiresArray);

            jsonObject.add("uid", new JsonPrimitive("me.deftware"));
            jsonObject.add("version", new JsonPrimitive(emcVersion));
        }
        return jsonObject;
    }

    @Override
    public JsonObject build(String mcVersion, String emcVersion, String launcherString, String modLoaderString, String subsystemVersion, String id, String emcTweaker, String inheritsFrom) {
        System.out.println("Incorrect JsonBuilder in Use, building using other method");
        return build(mcVersion, emcVersion, launcherString, modLoaderString, id, emcTweaker, inheritsFrom);
    }

}
