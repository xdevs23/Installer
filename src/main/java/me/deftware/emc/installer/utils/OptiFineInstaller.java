package me.deftware.emc.installer.utils;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.nio.charset.Charset;

public class OptiFineInstaller {

    public static void installOptifine(String optifineVersion, String mcVersion) throws Exception {
        File optifineDir = new File(Utils.getMinecraftRoot() + "versions" + File.separator + mcVersion + "-" + optifineVersion.replace("_" + mcVersion, "").replace(".jar", "").replace("preview_", ""));
        if (!optifineDir.exists()) {
            File optifine = new File(Utils.getMinecraftRoot() + "optifine_installer.jar");
            String website = WebUtils.get("https://optifine.net/adloadx?f=" + optifineVersion + ".jar");
            website = "https://optifine.net/downloadx" + website.split("downloadx")[1].split("'")[0];
            WebUtils.download(website, optifine.getAbsolutePath());
            System.out.println(IOUtils.toString(new ProcessBuilder(new String[]{"java", "-cp", optifine.getAbsolutePath(), "optifine.Installer"}).start().getInputStream(), Charset.defaultCharset()));
            if (!optifine.delete()) {
                System.out.println("Could not delete OptiFine installer jar");
            }
            System.out.println("Installed OptiFine");
        } else {
            System.out.println("OptiFine already installed, skipping...");
        }
    }

}
