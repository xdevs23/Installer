package me.deftware.emc.installer.utils.jsonbuilder.marketplace;

import com.google.gson.JsonObject;

import java.util.ArrayList;

public class MarketplaceElement {
    public String ID, name, author, description, link, sha1;
    public JsonObject rootData;
    public float version;
    public boolean isValid;

    public MarketplaceElement(JsonObject rootObject) {
        initData(rootObject);
    }

    public MarketplaceElement(String searchName, ArrayList<MarketplaceElement> targets) {
        for (MarketplaceElement element : targets) {
            if (element.name.equals(searchName)) {
                initData(element.rootData);
            }
        }
    }

    private void initData(JsonObject rootObject) {
        try {
            ID = rootObject.get("id").getAsString();
            name = rootObject.get("name").getAsString();
            author = rootObject.get("author").getAsString();
            description = rootObject.get("description").getAsString();
            version = rootObject.get("version").getAsFloat();
            link = rootObject.get("link").getAsString();
            sha1 = rootObject.get("sha1").getAsString();
            rootData = rootObject;

            isValid = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            isValid = false;
        }
    }
}
